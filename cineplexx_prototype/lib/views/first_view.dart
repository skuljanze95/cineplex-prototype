import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FirstView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              SizedBox(height: MediaQuery.of(context).size.width * 0.5),
              Expanded(
                child: AutoSizeText(
                  "slika",
                  textAlign: TextAlign.center,
                ),
              ),
              AutoSizeText(
                "Welcome to AP Films & Theatres",
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: const EdgeInsets.all(40.0),
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(8.0),
                    side: BorderSide(color: Colors.red),
                  ),
                  color: const Color(0xffFE2E2E),
                  child: Padding(
                    padding: const EdgeInsets.only(top: 15.0, bottom: 15.0),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(boxShadow: [
                        BoxShadow(
                          color: Color.fromRGBO(254, 46, 46, 0.15),
                          blurRadius:
                              18.0, // has the effect of softening the shadow
                          spreadRadius:
                              3.0, // has the effect of extending the shadow
                          offset: Offset(
                            0, // horizontal, move right 10
                            20.0, // vertical, move down 10
                          ),
                        )
                      ]),
                      child: Text(
                        "Get Started!",
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.of(context).pushReplacementNamed("/singIn");
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
