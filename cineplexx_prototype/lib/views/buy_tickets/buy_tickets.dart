import 'package:auto_size_text/auto_size_text.dart';
import 'package:cineplexx_prototype/views/buy_tickets/cart.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

enum state { selectTheathre, selectDate, selectSeats }

class BuyTickets extends StatefulWidget {
  String imdb_id;

  BuyTickets(this.imdb_id);
  @override
  _BuyTicketsState createState() => _BuyTicketsState(imdb_id);
}

class _BuyTicketsState extends State<BuyTickets> {
  String imdb_id;

  Map cartInfo = {
    /** Theather */ '0': '',
    /** Date */ '1': '',
    /** Time */ '2': '',
    /** Tickets_Number */ '3': ''
  };
  List tickets = <String>[];

  var date = "";

  var cinemas = {
    "0": <bool>[false],
    "1": <bool>[false],
    "2": <bool>[false],
    "3": <bool>[false],
    "4": <bool>[false],
  };

  var dates = {
    "0": <bool>[false],
    "1": <bool>[false],
    "2": <bool>[false],
    "3": <bool>[false],
    "4": <bool>[false],
    "5": <bool>[false],
    "6": <bool>[false],
    "7": <bool>[false],
    "8": <bool>[false],
    "9": <bool>[false],
    "10": <bool>[false],
    "11": <bool>[false],
    "12": <bool>[false],
    "13": <bool>[false],
    "14": <bool>[false],
  };

  var times = {
    "0": <bool>[false],
    "1": <bool>[false],
    "2": <bool>[false],
    "3": <bool>[false],
    "4": <bool>[false],
  };

  var seats = {
    "A1": <bool>[false],
    "A2": <bool>[false],
    "A3": <bool>[false],
    "A4": <bool>[false],
    "B1": <bool>[false],
    "B10": <bool>[false],
    "B2": <bool>[false],
    "B3": <bool>[false],
    "B4": <bool>[false],
    "B5": <bool>[false],
    "B6": <bool>[false],
    "B7": <bool>[false],
    "B8": <bool>[false],
    "B9": <bool>[false],
    "C1": <bool>[false],
    "C10": <bool>[false],
    "C2": <bool>[false],
    "C3": <bool>[false],
    "C4": <bool>[false],
    "C5": <bool>[false],
    "C6": <bool>[false],
    "C7": <bool>[false],
    "C8": <bool>[false],
    "C9": <bool>[false],
    "D1": <bool>[false],
    "D10": <bool>[false],
    "D2": <bool>[false],
    "D3": <bool>[false],
    "D4": <bool>[false],
    "D5": <bool>[false],
    "D6": <bool>[false],
    "D7": <bool>[false],
    "D8": <bool>[false],
    "D9": <bool>[false],
    "E1": <bool>[false],
    "E10": <bool>[false],
    "E2": <bool>[false],
    "E3": <bool>[false],
    "E4": <bool>[false],
    "E5": <bool>[false],
    "E6": <bool>[false],
    "E7": <bool>[false],
    "E8": <bool>[false],
    "E9": <bool>[false],
    "F1": <bool>[false],
    "F10": <bool>[false],
    "F2": <bool>[false],
    "F3": <bool>[false],
    "F4": <bool>[false],
    "F5": <bool>[false],
    "F6": <bool>[false],
    "F7": <bool>[false],
    "F8": <bool>[false],
    "F9": <bool>[false],
    "G1": <bool>[false],
    "G2": <bool>[false],
  };
  var curentState = state.selectTheathre;

  _BuyTicketsState(this.imdb_id);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              if (curentState == state.selectTheathre) {
                Navigator.of(context).pop();
              } else if (curentState == state.selectDate) {
                setState(() {
                  curentState = state.selectTheathre;
                });
              } else if (curentState == state.selectSeats) {
                setState(() {
                  curentState = state.selectDate;
                });
              }
            },
          ),
          centerTitle: true,
          title: Text("data"),
        ),
        body: Container(
          child: Column(
            children: <Widget>[
              FutureBuilder(
                future: buildTicketCart(imdb_id),
                builder:
                    (BuildContext context, AsyncSnapshot<Widget> snapshot) {
                  if (snapshot.hasData) return snapshot.data;

                  return Container();
                },
              ),
              Flexible(child: stateViewControler()),
              FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(8.0),
                  side: BorderSide(color: Colors.red),
                ),
                child: Text("Next"),
                onPressed: () {
                  if (curentState == state.selectTheathre) {
                    setState(() {
                      if (cartInfo["0"].isEmpty) {
                        Flushbar(
                          title: "Error",
                          message: "Select Cinema ",
                          duration: Duration(seconds: 3),
                        )..show(context);
                      } else {
                        curentState = state.selectDate;
                      }
                    });
                  } else if (curentState == state.selectDate) {
                    setState(() {
                      if (cartInfo["1"].isEmpty) {
                        Flushbar(
                          title: "Error",
                          message: "Select Date",
                          duration: Duration(seconds: 3),
                        )..show(context);
                      } else if (cartInfo["2"].isEmpty) {
                        Flushbar(
                          title: "Error",
                          message: "Select  Time",
                          duration: Duration(seconds: 3),
                        )..show(context);
                      } else {
                        curentState = state.selectSeats;
                      }
                    });
                  } else if (curentState == state.selectSeats) {
                    if (tickets.isEmpty) {
                      Flushbar(
                        title: "Error",
                        message: "Select seats",
                        duration: Duration(seconds: 3),
                      )..show(context);
                    } else {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                CartView(cartInfo, tickets, imdb_id, date)),
                      );
                    }
                  }
                },
              ),
            ],
          ),
        ));
  }

  stateViewControler() {
    if (curentState == state.selectTheathre) {
      return buildSelectTheathre();
    } else if (curentState == state.selectDate) {
      return buildSelectDate(cartInfo["0"], imdb_id);
    } else if (curentState == state.selectSeats) {
      return buildSelectSeats();
    }
  }

  buildSelectSeats() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text("Select Seats"),
          StreamBuilder(
              stream: Firestore.instance
                  .collection("cinemas")
                  .document("Cinemax 3D ${cartInfo["0"]}")
                  .collection(imdb_id)
                  .document(date)
                  .collection("time")
                  .document(cartInfo["2"])
                  .collection("seats")
                  .document("Seats")
                  .snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return Text("Nalagam...");
                } else {
                  return Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            buildSeatsRowB1("G", snapshot),
                            buildSeatsRow1("F", snapshot),
                            buildSeatsRow1("E", snapshot),
                            buildSeatsRow1("D", snapshot),
                            buildSeatsRow1("C", snapshot),
                            buildSeatsRow1("B", snapshot),
                            buildSeatsRowT1("A", snapshot),
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            buildSeatsRow2("F", snapshot),
                            buildSeatsRow2("E", snapshot),
                            buildSeatsRow2("D", snapshot),
                            buildSeatsRow2("C", snapshot),
                            buildSeatsRow2("B", snapshot),
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            buildSeatsRowB3("G", snapshot),
                            buildSeatsRow3("F", snapshot),
                            buildSeatsRow3("E", snapshot),
                            buildSeatsRow3("D", snapshot),
                            buildSeatsRow3("C", snapshot),
                            buildSeatsRow3("B", snapshot),
                            buildSeatsRowT3("A", snapshot),
                          ],
                        ),
                      ],
                    ),
                  );
                }
              })
        ],
      ),
    );
  }

  Row buildSeatsRow1(row, var snapshot) {
    return Row(
      children: <Widget>[
        buildSeat(row + "1", snapshot),
        buildSeat(row + "2", snapshot),
        buildSeat(row + "3", snapshot),
      ],
    );
  }

  Row buildSeatsRowB1(row, var snapshot) {
    return Row(
      children: <Widget>[
        buildSeat(row + "1", snapshot),
        buildSpaceContainer(),
        buildSpaceContainer(),
      ],
    );
  }

  Row buildSeatsRowT1(row, var snapshot) {
    return Row(
      children: <Widget>[
        buildSeat(row + "1", snapshot),
        buildSeat(row + "2", snapshot),
        buildSpaceContainer(),
      ],
    );
  }

  Row buildSeatsRow2(row, var snapshot) {
    return Row(
      children: <Widget>[
        buildSeat(row + "4", snapshot),
        buildSeat(row + "5", snapshot),
        buildSeat(row + "6", snapshot),
        buildSeat(row + "7", snapshot),
      ],
    );
  }

  Row buildSeatsRow3(row, var snapshot) {
    return Row(
      children: <Widget>[
        buildSeat(row + "8", snapshot),
        buildSeat(row + "9", snapshot),
        buildSeat(row + "10", snapshot),
      ],
    );
  }

  Row buildSeatsRowB3(row, var snapshot) {
    return Row(
      children: <Widget>[
        buildSpaceContainer(),
        buildSpaceContainer(),
        buildSeat(row + "2", snapshot),
      ],
    );
  }

  Row buildSeatsRowT3(row, var snapshot) {
    return Row(
      children: <Widget>[
        buildSpaceContainer(),
        buildSeat(row + "3", snapshot),
        buildSeat(row + "4", snapshot),
      ],
    );
  }

  buildSpaceContainer() {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Container(
        height: 30,
        width: 30,
      ),
    );
  }

  buildSeat(row, var snapshot) {
    var color;
    if (snapshot.data.data[row] == "Empty") {
      color = Colors.white;
    } else if (snapshot.data.data[row] == "Reserved") {
      color = Colors.grey;
    } else if (snapshot.data.data[row] == "InProgres") {
      color = Colors.black12;
    }
    return InkWell(
      onTap: () {
        if (snapshot.data.data[row] == "Empty" &&
            seats[row][0] == false &&
            tickets.length < 8) {
          tickets.add(row);

          setState(() {
            seats[row][0] = !seats[row][0];
            cartInfo["3"] = "${tickets.length} Tickets";
          });
        } else if (seats[row][0] == true) {
          tickets.remove(row);
          setState(() {
            seats[row][0] = !seats[row][0];
            cartInfo["3"] = "${tickets.length} Tickets";
          });
        }

        //  value = !value;
        // print(value);
      },
      child: Padding(
        padding: const EdgeInsets.all(4.0),
        child: Container(
          decoration: BoxDecoration(
              color: seats[row][0] ? Colors.red : color,
              borderRadius: BorderRadius.circular(10),
              border: Border.all(width: 1)),
          height: 30,
          width: 30,
          child: Center(child: Text(row)),
        ),
      ),
    );
  }

  buildSelectDate(String city, imdb_id) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Text("Select Date"),
        Flexible(
          child: StreamBuilder(
              stream: Firestore.instance
                  .collection("cinemas")
                  .document("Cinemax 3D $city")
                  .collection(imdb_id)
                  .snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return Text("Nalagam...");
                } else {
                  return GridView.builder(
                      itemCount: 7,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 5,
                          childAspectRatio: 1,
                          crossAxisSpacing: 18),
                      itemBuilder: (BuildContext context, int index) {
                        DocumentSnapshot docsSnap =
                            snapshot.data.documents[index];

                        return InkWell(
                          onTap: () {
                            setState(() {
                              dates.forEach((k, v) {
                                dates[k][0] = false;
                              });
                              dates["$index"][0] = !dates["$index"][0];
                              cartInfo["1"] =
                                  docsSnap["Day"] + " " + docsSnap["DayN"];
                              date = docsSnap.documentID;
                            });
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(1),
                            child: Container(
                              color: dates["$index"][0]
                                  ? Colors.red
                                  : Colors.white,
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  AutoSizeText(
                                    docsSnap["Month"],
                                    style: TextStyle(fontSize: 14),
                                    maxLines: 2,
                                    minFontSize: 9,
                                  ),
                                  AutoSizeText(
                                    docsSnap["DayN"],
                                    style: TextStyle(fontSize: 20),
                                    maxLines: 2,
                                    minFontSize: 9,
                                  ),
                                  AutoSizeText(
                                    docsSnap["Day"],
                                    style: TextStyle(fontSize: 9),
                                    maxLines: 2,
                                    minFontSize: 9,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      });
                }
              }),
        ),
        Text("Select Time"),
        Flexible(child: buildSelectTimeRow(city, imdb_id)),
      ],
    );
  }

  Row buildSelectTimeRow(String city, imdb_id) {
    if (date == "") {
      return Row();
    } else {
      return Row(
        children: <Widget>[
          Flexible(
            child: Container(
              child: StreamBuilder(
                  stream: Firestore.instance
                      .collection("cinemas")
                      .document("Cinemax 3D $city")
                      .collection(imdb_id)
                      .document(date)
                      .collection("time")
                      .snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return Text("Nalagam...");
                    } else {
                      return GridView.builder(
                          itemCount: 5,
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 3,
                                  childAspectRatio: 2,
                                  crossAxisSpacing: 18),
                          itemBuilder: (BuildContext context, int index) {
                            DocumentSnapshot docsSnap =
                                snapshot.data.documents[index];

                            return InkWell(
                              onTap: () {
                                setState(() {
                                  times.forEach((k, v) {
                                    times[k][0] = false;
                                  });
                                  times["$index"][0] = !times["$index"][0];
                                  cartInfo["2"] = docsSnap.documentID;
                                });
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  height: 100,
                                  color: times["$index"][0]
                                      ? Colors.red
                                      : Colors.white,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Center(
                                        child: AutoSizeText(
                                          docsSnap.documentID,
                                          maxLines: 2,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          });
                    }
                  }),
            ),
          ),
        ],
      );
    }
  }

  buildSelectTheathre() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text("Select Cinema"),
        Expanded(
          child: StreamBuilder(
              stream: Firestore.instance.collection("cinemas").snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return Text("Nalagam...");
                } else {
                  return GridView.builder(
                      itemCount: snapshot.data.documents.length,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        childAspectRatio: 4,
                      ),
                      itemBuilder: (BuildContext context, int index) {
                        DocumentSnapshot docsSnap =
                            snapshot.data.documents[index];

                        return InkWell(
                          onTap: () {
                            setState(() {
                              cinemas.forEach((k, v) {
                                cinemas[k][0] = false;
                              });
                              cinemas["$index"][0] = !cinemas["$index"][0];
                              cartInfo["0"] = docsSnap["City"];
                            });
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              height: 100,
                              color: cinemas["$index"][0]
                                  ? Colors.red
                                  : Colors.white,
                              child: Column(
                                children: <Widget>[
                                  AutoSizeText(
                                    docsSnap["Name"],
                                    maxLines: 2,
                                  ),
                                  AutoSizeText(
                                    docsSnap["City"],
                                    maxLines: 2,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      });
                }
              }),
        )
      ],
    );
  }

  Future<Container> buildTicketCart(imdb_id) async {
    var url = 'http://www.omdbapi.com/?i=$imdb_id&apikey=97ad91ca';
    var response = await http.get(url);
    var genres, title, img;

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);

      genres = jsonResponse['Genre'];
      title = jsonResponse['Title'];
      img = jsonResponse['Poster'];
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }

    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(22.0, 22.0, 7.0, 22.0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(12),
              child: Image.network(
                img,
                height: 160.0,
                fit: BoxFit.fitWidth,
              ),
            ),
          ),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(7.0, 40.0, 15.0, 22.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  AutoSizeText(
                    title,
                    style: TextStyle(fontSize: 20),
                  ),
                  SizedBox(height: 5),
                  AutoSizeText(
                    genres,
                  ),
                  SizedBox(height: 5),
                  Container(
                    child: GridView.builder(
                      shrinkWrap: true,
                      itemCount: cartInfo.length,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          childAspectRatio: 4, crossAxisCount: 2),
                      itemBuilder: (BuildContext context, int index) {
                        return buildCartCards(index);
                      },
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  buildCartCards(int index) {
    if (cartInfo["$index"] != '') {
      return Padding(
        padding: const EdgeInsets.all(4.0),
        child: Container(
          color: Colors.red,
          child: Center(
            child: Text(
              cartInfo["$index"].toString(),
            ),
          ),
        ),
      );
    } else {
      return Container();
    }
  }
}
