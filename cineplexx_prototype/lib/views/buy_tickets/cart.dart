import 'dart:async';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cineplexx_prototype/views/main_view/main_view.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'dart:math' as math;

class CartView extends StatefulWidget {
  var cartInfo;
  var tickets;
  var imdb_id;
  var date;

  CartView(this.cartInfo, this.tickets, this.imdb_id, this.date);

  @override
  _CartViewState createState() =>
      _CartViewState(cartInfo, tickets, imdb_id, date);
}

class _CartViewState extends State<CartView> with WidgetsBindingObserver {
  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    setEmpty();
    Navigator.pop(context);
  }

  var cartInfo;
  var tickets;
  var imdb_id;
  var date;

  _CartViewState(
    this.cartInfo,
    this.tickets,
    this.imdb_id,
    this.date,
  );

  @override
  Widget build(BuildContext context) {
    Timer(Duration(seconds: 300), () {
      setEmpty();
      Navigator.pop(context, false);
    });
    setInProgres();
    return WillPopScope(
      onWillPop: () {
        setEmpty();

        //trigger leaving and use own data
        Navigator.pop(context, false);

        //we need to return a future
        return Future.value(false);
      },
      child: Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Color(0xffFAFAFA),
          automaticallyImplyLeading: false,
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                setEmpty();
                Navigator.of(context).pop();
              }),
          centerTitle: true,
          title: Text("Confirm & Pay"),
        ),
        body: SafeArea(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 18.0, right: 18.0),
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(12),
                                topRight: Radius.circular(12)),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black12,
                                blurRadius:
                                    15.0, // has the effect of softening the shadow
                                spreadRadius:
                                    6.0, // has the effect of extending the shadow
                                offset: Offset(
                                  0, // horizontal, move right 10
                                  8.0, // vertical, move down 10
                                ),
                              )
                            ]),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.43,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      buildInputName("First name"),
                                      buildInputField(
                                        "_first_name",
                                        "Enter first name",
                                        22.0,
                                        0.0,
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.43,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 3, top: 22.0, bottom: 8.0),
                                        child: Text("Last name"),
                                      ),
                                      buildInputField("_last_name",
                                          "Enter last name", 0.0, 22.0),
                                    ],
                                  ),
                                )
                              ],
                            ),
                            buildInputName("Mobile number"),
                            buildInputField(
                              "_mobile_number",
                              "Enter your number",
                              22.0,
                              22.0,
                            ),
                            buildInputName("Email"),
                            buildInputField(
                              "_email",
                              "Enter email address",
                              22.0,
                              22.0,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 18.0, right: 18.0),
                      child: Container(
                        color: Colors.white,
                        child: Column(
                          children: <Widget>[
                            Container(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    decoration: BoxDecoration(
                                      color: Color(0xffF1F1F1),
                                      borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(100)),
                                    ),
                                    width: 10.0,
                                    height: 10.0,
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                      color: Color(0xffF1F1F1),
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(100)),
                                    ),
                                    width: 10.0,
                                    height: 10.0,
                                  )
                                ],
                              ),
                            ),
                            Container(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    decoration: BoxDecoration(
                                      color: Color(0xffF1F1F1),
                                      borderRadius: BorderRadius.only(
                                          bottomRight: Radius.circular(100)),
                                    ),
                                    width: 10.0,
                                    height: 10.0,
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                      color: Color(0xffF1F1F1),
                                      borderRadius: BorderRadius.only(
                                          bottomLeft: Radius.circular(100)),
                                    ),
                                    width: 10.0,
                                    height: 10.0,
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 18.0, right: 18.0),
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(12),
                                bottomRight: Radius.circular(12)),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Color(0xffF1F1F1),
                                blurRadius:
                                    15.0, // has the effect of softening the shadow
                                spreadRadius:
                                    6.0, // has the effect of extending the shadow
                                offset: Offset(
                                  0, // horizontal, move right 10
                                  18.0, // vertical, move down 10
                                ),
                              )
                            ]),
                        child: FutureBuilder(
                          future: buildTicketCart(widget.imdb_id),
                          builder: (BuildContext context,
                              AsyncSnapshot<Widget> snapshot) {
                            if (snapshot.hasData) return snapshot.data;

                            return Container();
                          },
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    RaisedButton(
                      onPressed: () {
                        setEmpty();
                        Navigator.of(context).pop();
                      },
                      child: Text("Back"),
                    ),
                    RaisedButton(
                      onPressed: () {
                        Navigator.of(context).pushNamedAndRemoveUntil(
                            '/home', (Route<dynamic> route) => false);

                        setReserved();
                      },
                      child: Text("Pay Now"),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<Container> buildTicketCart(imdb_id) async {
    var url = 'http://www.omdbapi.com/?i=$imdb_id&apikey=97ad91ca';
    var response = await http.get(url);
    var genres, title, img;

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);

      genres = jsonResponse['Genre'];
      title = jsonResponse['Title'];
      img = jsonResponse['Poster'];
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }

    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(22.0, 22.0, 7.0, 22.0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(12),
              child: Image.network(
                img,
                height: 160.0,
                fit: BoxFit.fitWidth,
              ),
            ),
          ),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(7.0, 40.0, 15.0, 22.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  AutoSizeText(
                    title,
                    style: TextStyle(fontSize: 20),
                  ),
                  SizedBox(height: 5),
                  AutoSizeText(
                    genres,
                  ),
                  SizedBox(height: 5),
                  Container(
                    child: GridView.builder(
                      shrinkWrap: true,
                      itemCount: widget.cartInfo.length,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          childAspectRatio: 4, crossAxisCount: 2),
                      itemBuilder: (BuildContext context, int index) {
                        return buildCartCards(index);
                      },
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  buildCartCards(int index) {
    if (widget.cartInfo["$index"] != '') {
      return Padding(
        padding: const EdgeInsets.all(4.0),
        child: Container(
          color: Colors.red,
          child: Center(
            child: Text(
              widget.cartInfo["$index"].toString(),
            ),
          ),
        ),
      );
    } else {
      return Container();
    }
  }

  void setInProgres() {
    tickets.forEach((k) {
      Firestore.instance
          .collection("cinemas")
          .document("Cinemax 3D ${cartInfo["0"]}")
          .collection(imdb_id)
          .document(date)
          .collection("time")
          .document(cartInfo["2"])
          .collection("seats")
          .document("Seats")
          .updateData({k: "InProgres"});
    });
  }

  void setEmpty() {
    tickets.forEach((k) {
      Firestore.instance
          .collection("cinemas")
          .document("Cinemax 3D ${cartInfo["0"]}")
          .collection(imdb_id)
          .document(date)
          .collection("time")
          .document(cartInfo["2"])
          .collection("seats")
          .document("Seats")
          .updateData({k: "Empty"});
    });
  }

  void setReserved() {
    tickets.forEach((k) {
      Firestore.instance
          .collection("cinemas")
          .document("Cinemax 3D ${cartInfo["0"]}")
          .collection(imdb_id)
          .document(date)
          .collection("time")
          .document(cartInfo["2"])
          .collection("seats")
          .document("Seats")
          .updateData({k: "Reserved"});
    });
  }

  buildInputName(String name) {
    return Padding(
      padding: const EdgeInsets.only(left: 25.0, top: 22.0, bottom: 8.0),
      child: Text(name),
    );
  }

  buildInputField(name, hint, left, right) {
    return Padding(
      padding: EdgeInsets.only(left: left, right: right),
      child: TextFormField(
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
          hintText: hint,
          focusColor: Colors.grey,
          focusedBorder: OutlineInputBorder(
              borderRadius: const BorderRadius.all(
                const Radius.circular(9.0),
              ),
              borderSide: BorderSide(color: Colors.grey)),
          border: OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(9.0),
            ),
            borderSide: BorderSide(color: Colors.grey),
          ),
        ),
        onChanged: (value) {},
      ),
    );
  }
}
