import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';

import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

import 'buy_tickets/buy_tickets.dart';

class MovieView extends StatelessWidget {
  String imdb_id;

  MovieView(this.imdb_id);
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.red),
        centerTitle: true,
        title: Text(
          "PT Movies",
          style: TextStyle(color: Colors.red),
        ),
        backgroundColor: Colors.white,
        elevation: 0.0,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height * 0.55,
                child: Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.center,
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black12,
                                blurRadius:
                                    18.0, // has the effect of softening the shadow
                                spreadRadius:
                                    3.0, // has the effect of extending the shadow
                                offset: Offset(
                                  0, // horizontal, move right 10
                                  6.0, // vertical, move down 10
                                ),
                              )
                            ]),
                        height: MediaQuery.of(context).size.height * 0.4,
                        width: MediaQuery.of(context).size.width * 0.90,
                      ),
                    ),
                    Center(
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black12,
                                blurRadius:
                                    18.0, // has the effect of softening the shadow
                                spreadRadius:
                                    3.0, // has the effect of extending the shadow
                                offset: Offset(
                                  0, // horizontal, move right 10
                                  6.0, // vertical, move down 10
                                ),
                              )
                            ]),
                        height: MediaQuery.of(context).size.height * 0.45,
                        width: MediaQuery.of(context).size.width * 0.80,
                      ),
                    ),
                    Center(
                      child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black12,
                                  blurRadius:
                                      18.0, // has the effect of softening the shadow
                                  spreadRadius:
                                      3.0, // has the effect of extending the shadow
                                  offset: Offset(
                                    0, // horizontal, move right 10
                                    6.0, // vertical, move down 10
                                  ),
                                )
                              ]),
                          height: MediaQuery.of(context).size.height * 0.5,
                          width: MediaQuery.of(context).size.width * 0.70,
                          child: FutureBuilder(
                              future: buildTrailer(imdb_id, height, width),
                              builder: (BuildContext context,
                                  AsyncSnapshot<Widget> snapshot) {
                                if (snapshot.hasData) return snapshot.data;

                                return Container();
                              })),
                    )
                  ],
                ),
              ),
              RaisedButton(
                elevation: 0.0,
                color: const Color(0xffFE2E2E),
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(8.0),
                ),
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                child: Container(
                  width: width * 0.60,
                  height: height * 0.055,
                  decoration: BoxDecoration(boxShadow: [
                    BoxShadow(
                      color: Color.fromRGBO(254, 46, 46, 0.15),
                      blurRadius:
                          18.0, // has the effect of softening the shadow
                      spreadRadius:
                          3.0, // has the effect of extending the shadow
                      offset: Offset(
                        0, // horizontal, move right 10
                        15.0, // vertical, move down 10
                      ),
                    )
                  ]),
                  child: Center(
                    child: Text(
                      "Buy Tickets",
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
                onPressed: () {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (context) => BuyTickets(imdb_id)),
                  );
                },
              ),
              Padding(
                padding: const EdgeInsets.all(28.0),
                child: FutureBuilder(
                  future: buildInfo(imdb_id, height, width),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if (snapshot.hasData) return snapshot.data;

                    return Column();
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<Column> buildTrailer(id, var height, width) async {
    var url = 'http://www.omdbapi.com/?i=$id&apikey=97ad91ca';
    var response = await http.get(url);
    var title;
    var img;

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);

      //print(jsonResponse);
      title = jsonResponse['Title'];
      img = jsonResponse['Poster'];
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }

    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Container(
          child: Stack(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: Image.network(
                  img,
                  width: width * 0.6,
                  height: height * 0.4,
                  fit: BoxFit.fitWidth,
                ),
              ),
              Column(
                children: <Widget>[
                  RaisedButton(
                    onPressed: () {},
                  ),
                  Text("Play Trailer")
                ],
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(22.0, 0, 22.0, 0),
          child: AutoSizeText(
            title,
            maxLines: 2,
          ),
        ),
      ],
    );
  }

  Future<Column> buildInfo(id, var height, width) async {
    var url = 'http://www.omdbapi.com/?i=$id&apikey=97ad91ca';
    var response = await http.get(url);
    var genres, directors, cast, plot;

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);

      genres = jsonResponse['Genre'];
      directors = jsonResponse['Director'];
      cast = jsonResponse['Actors'].split(',');
      plot = jsonResponse['Plot'];
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }

    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Row(
          children: <Widget>[AutoSizeText("Genres: "), AutoSizeText(genres)],
        ),
        Row(
          children: <Widget>[
            AutoSizeText("Director: "),
            AutoSizeText(directors)
          ],
        ),
        Column(
          children: <Widget>[
            Column(
              children: <Widget>[
                AutoSizeText("Actors: "),
                Container(
                  height: 100,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: cast.length,
                    itemBuilder: (BuildContext context, int index) {
                      return new Column(
                        children: <Widget>[
                          FutureBuilder(
                            future: getActorPicture(cast[index]),
                            builder:
                                (BuildContext context, AsyncSnapshot snapshot) {
                              if (snapshot.hasData) return snapshot.data;

                              return Container();
                            },
                          ),
                          Container(
                            width: 56.0,
                            child: AutoSizeText(
                              cast[index],
                              maxLines: 2,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 12,
                              ),
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                ),
                AutoSizeText(plot),
              ],
            ),
          ],
        ),
      ],
    );
  }

  Future<Container> getActorPicture(name) async {
    var url =
        'https://api.themoviedb.org/3/search/person?api_key=85ad6c9c1e79b127026e8d2270b8110d&language=en-US&query=$name&page=1&include_adult=false';
    var response = await http.get(url);
    var picture;
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);

      picture = jsonResponse['results'][0]['profile_path'];
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }

    return Container(
      width: 50.0,
      height: 50.0,
      decoration: new BoxDecoration(
        shape: BoxShape.circle,
        image: new DecorationImage(
          fit: BoxFit.cover,
          image: new NetworkImage("https://image.tmdb.org/t/p/w500$picture"),
        ),
      ),
    );
  }
}
