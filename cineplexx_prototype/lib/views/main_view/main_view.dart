import 'package:cineplexx_prototype/views/main_view/tabs/in_theatres.dart';
import 'package:cineplexx_prototype/views/main_view/tabs/news_view.dart';
import 'package:cineplexx_prototype/views/main_view/tabs/settings.dart';
import 'package:cineplexx_prototype/views/main_view/tabs/theatres.dart';
import 'package:cineplexx_prototype/views/main_view/tabs/up_coming.dart';
import 'package:flutter/material.dart';

class MainView extends StatefulWidget {
  @override
  _MainViewState createState() => _MainViewState();
}

class _MainViewState extends State<MainView> {
  int _currentIndex = 0;
  final List<Widget> _children = [
    InTheatresView(),
    UpComingView(),
    TheatresView(),
    NewsView(),
    SettingsView(),
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color(0xffFCFCFC),
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            elevation: 0.0,
            actions: <Widget>[
              Icon(
                Icons.camera_alt,
                color: Colors.red,
              )
            ],
            backgroundColor: Color(0xffFCFCFC),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "PT Movies",
                  style: TextStyle(color: Colors.red),
                ),
              ],
            ),
          ),
          body: _children[_currentIndex], // new
          bottomNavigationBar: BottomNavigationBar(
            backgroundColor: Color(0xffFCFCFC),
            elevation: 0.0,
            type: BottomNavigationBarType.fixed,
            unselectedFontSize: 10,
            selectedFontSize: 10,
            showUnselectedLabels: true,
            iconSize: 34.0,
            unselectedItemColor: Colors.blue,
            fixedColor: Colors.red,

            onTap: onTabTapped, // new
            currentIndex: _currentIndex, // new
            items: [
              new BottomNavigationBarItem(
                icon: Icon(Icons.theaters),
                title: Text('In Theatres'),
              ),
              new BottomNavigationBarItem(
                icon: Icon(Icons.camera_alt),
                title: Text('Up Coming'),
              ),
              new BottomNavigationBarItem(
                icon: Icon(Icons.store_mall_directory),
                title: Text(
                  'Theatres',
                ),
              ),
              new BottomNavigationBarItem(
                icon: Icon(Icons.perm_phone_msg),
                title: Text('News'),
              ),
              new BottomNavigationBarItem(
                icon: Icon(Icons.settings),
                title: Text('Settings'),
              )
            ],
          ),
        ),
      ),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}
