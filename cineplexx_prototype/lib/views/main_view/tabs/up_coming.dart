import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class UpComingView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final WebSocketLink webSocketLink =
        WebSocketLink(url: 'ws://cineplexx-prototype.herokuapp.com/v1/graphql');

    final ValueNotifier<GraphQLClient> client = ValueNotifier<GraphQLClient>(
      GraphQLClient(
        link: webSocketLink,
        cache: OptimisticCache(
          dataIdFromObject: typenameDataIdFromObject,
        ),
      ),
    );
    return GraphQLProvider(
      client: client,
      child: CacheProvider(
        child: UpComing_G(),
      ),
    );
  }
}

class UpComing_G extends StatefulWidget {
  @override
  _Upcoming_GState createState() => _Upcoming_GState();
}

class _Upcoming_GState extends State<UpComing_G> {
  String query = '''
  query MyQuery {
 
  up_coming {
    name
    id
  }
}
  ''';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment(20, 29),
            child: Container(
              color: Colors.white,
              height: MediaQuery.of(context).size.height * 0.9,
              child: Subscription<Map<String, dynamic>>(
                'MyQuery',
                query,
                builder: ({dynamic loading, dynamic payload, dynamic error}) {
                  if (error != null) {
                    return Text(error.toString());
                  }

                  if (loading == true) {
                    return Center(
                      child: const CircularProgressIndicator(),
                    );
                  }
                  return StaggeredGridView.countBuilder(
                    crossAxisCount: 4,
                    itemCount: payload['up_coming'].length,
                    itemBuilder: (BuildContext context, int index) {
                      return Column(
                        children: <Widget>[
                          FutureBuilder(
                              future: buildNameText(payload, index),
                              builder: (BuildContext context,
                                  AsyncSnapshot<Widget> snapshot) {
                                if (snapshot.hasData) return snapshot.data;

                                return Container();
                              })
                        ],
                      );
                    },
                    staggeredTileBuilder: (int index) =>
                        new StaggeredTile.fit(2),
                    mainAxisSpacing: 10.0,
                    crossAxisSpacing: 1.0,
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<Container> buildNameText(payload, int index) async {
    var idc = payload['up_coming'][index]['id'].toString();
    var url = 'http://www.omdbapi.com/?i=$idc&apikey=97ad91ca';
    var response = await http.get(url);
    var title;
    var img;

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);

      //print(jsonResponse);
      title = jsonResponse['Title'];
      img = jsonResponse['Poster'];
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }
    return Container(
        height: 320,
        child: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(boxShadow: [
                BoxShadow(
                  color: Colors.black12,
                  blurRadius: 18.0, // has the effect of softening the shadow
                  spreadRadius: 3.0, // has the effect of extending the shadow
                  offset: Offset(
                    0, // horizontal, move right 10
                    6.0, // vertical, move down 10
                  ),
                )
              ]),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: Image.network(
                  img,
                  height: 270.0,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.4,
                child: Align(
                  alignment: Alignment.topCenter,
                  child: AutoSizeText(title.toString(),
                      maxLines: 2,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 14.0,
                        //You can set your custom height here
                      )),
                ),
              ),
            ),
          ],
        ));
  }
}
