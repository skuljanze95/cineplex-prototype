import 'package:cineplexx_prototype/services/auth.dart';
import 'package:flutter/material.dart';

class SettingsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(11.0, 0, 11.0, 25.0),
                child: Container(
                  padding: EdgeInsets.only(bottom: 11.0),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black12,
                          blurRadius:
                              18.0, // has the effect of softening the shadow
                          spreadRadius:
                              3.0, // has the effect of extending the shadow
                          offset: Offset(
                            0, // horizontal, move right 10
                            6.0, // vertical, move down 10
                          ),
                        )
                      ]),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(
                          top: 22.0,
                          left: 30.0,
                          bottom: 8.0,
                        ),
                        child: Text(
                          "Account",
                          style: TextStyle(color: Colors.red, fontSize: 17.0),
                        ),
                      ),
                      buildSettingsRow("Change Password"),
                      buildSettingsRow("Change Mobile Number"),
                      buildSettingsRow("Two-Step Verification"),
                      buildSettingsRow("Change Email"),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(11.0, 0, 11.0, 25.0),
                child: Container(
                  padding: EdgeInsets.only(bottom: 11.0),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black12,
                          blurRadius:
                              18.0, // has the effect of softening the shadow
                          spreadRadius:
                              3.0, // has the effect of extending the shadow
                          offset: Offset(
                            0, // horizontal, move right 10
                            6.0, // vertical, move down 10
                          ),
                        )
                      ]),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(
                          top: 22.0,
                          left: 30.0,
                          bottom: 8.0,
                        ),
                        child: Text(
                          "Support",
                          style: TextStyle(color: Colors.red, fontSize: 17.0),
                        ),
                      ),
                      buildSettingsRow("Careers"),
                      buildSettingsRow("Help and Support"),
                      buildSettingsRow("About Us"),
                      buildSettingsRow("Contact Us"),
                      buildSettingsRow("Privacy Policy"),
                      buildSettingsRow("Terms of Use"),
                      buildSettingsRow("Disclaimer"),
                      FlatButton(
                        padding: EdgeInsets.only(left: 30),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "Logout",
                              style: TextStyle(
                                  fontSize: 17, fontWeight: FontWeight.w400),
                            ),
                          ],
                        ),
                        onPressed: () {
                          AuthService().signOut();
                        },
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  buildSettingsRow(String name) {
    return FlatButton(
      padding: EdgeInsets.only(left: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            name,
            style: TextStyle(fontSize: 17, fontWeight: FontWeight.w400),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 30.0),
            child: Icon(Icons.arrow_forward_ios),
          )
        ],
      ),
      onPressed: () {},
    );
  }
}
