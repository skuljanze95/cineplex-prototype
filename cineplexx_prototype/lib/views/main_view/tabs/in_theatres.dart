import 'package:auto_size_text/auto_size_text.dart';
import 'package:cineplexx_prototype/views/buy_tickets/buy_tickets.dart';
import 'package:cineplexx_prototype/views/movie_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class InTheatresView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final WebSocketLink webSocketLink =
        WebSocketLink(url: 'ws://cineplexx-prototype.herokuapp.com/v1/graphql');

    final ValueNotifier<GraphQLClient> client = ValueNotifier<GraphQLClient>(
      GraphQLClient(
        link: webSocketLink,
        cache: OptimisticCache(
          dataIdFromObject: typenameDataIdFromObject,
        ),
      ),
    );
    return GraphQLProvider(
      client: client,
      child: CacheProvider(
        child: InTheatresView_G(),
      ),
    );
  }
}

class InTheatresView_G extends StatefulWidget {
  @override
  _InTheatresView_G createState() => _InTheatresView_G();
}

class _InTheatresView_G extends State<InTheatresView_G> {
  String query1 = '''
  query MyQuery {
 
  in_theatres {
    name
    id
  }
}
  ''';
  String query = '''subscription MySubscription {
 
  in_theatres {
    name
    id
  }
}''';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            color: Colors.white,
            height: MediaQuery.of(context).size.height * 0.9,
            child: Subscription<Map<String, dynamic>>(
              'MySubscription',
              query,
              builder: ({dynamic loading, dynamic payload, dynamic error}) {
                if (error != null) {
                  return Text(error.toString());
                }

                if (loading == true) {
                  return Center(
                    child: const CircularProgressIndicator(),
                  );
                }
                return StaggeredGridView.countBuilder(
                  crossAxisCount: 4,
                  itemCount: payload['in_theatres'].length,
                  itemBuilder: (BuildContext context, int index) {
                    return Column(
                      children: <Widget>[
                        FutureBuilder(
                            future: buildNameText(payload, index),
                            builder: (BuildContext context,
                                AsyncSnapshot<Widget> snapshot) {
                              if (snapshot.hasData) return snapshot.data;

                              return Container();
                            })
                      ],
                    );
                  },
                  staggeredTileBuilder: (int index) => new StaggeredTile.fit(2),
                  mainAxisSpacing: 12.0,
                  crossAxisSpacing: 1.0,
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Future<Container> buildNameText(payload, int index) async {
    var idc = payload['in_theatres'][index]['id'].toString();
    var url = 'http://www.omdbapi.com/?i=$idc&apikey=97ad91ca';
    var response = await http.get(url);
    var title;
    var img;

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);

      //print(jsonResponse);
      title = jsonResponse['Title'];
      img = jsonResponse['Poster'];
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }
    return Container(
        height: 360,
        child: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(boxShadow: [
                BoxShadow(
                  color: Colors.black12,
                  blurRadius: 18.0, // has the effect of softening the shadow
                  spreadRadius: 3.0, // has the effect of extending the shadow
                  offset: Offset(
                    0, // horizontal, move right 10
                    6.0, // vertical, move down 10
                  ),
                )
              ]),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MovieView(idc)),
                    );
                  },
                  child: Image.network(
                    img,
                    height: 270.0,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.4,
                height: 40.0,
                child: Align(
                  alignment: Alignment.topCenter,
                  child: AutoSizeText(title.toString(),
                      maxLines: 2,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 14.0,
                        //You can set your custom height here
                      )),
                ),
              ),
            ),
            RaisedButton(
              elevation: 0.0,
              color: const Color(0xffFE2E2E),
              shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(8.0),
              ),
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              child: Container(
                width: MediaQuery.of(context).size.width * 0.36,
                decoration: BoxDecoration(boxShadow: [
                  BoxShadow(
                    color: Color.fromRGBO(254, 46, 46, 0.15),
                    blurRadius: 18.0, // has the effect of softening the shadow
                    spreadRadius: 3.0, // has the effect of extending the shadow
                    offset: Offset(
                      0, // horizontal, move right 10
                      15.0, // vertical, move down 10
                    ),
                  )
                ]),
                child: Text(
                  "Buy Tickets",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white),
                ),
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => BuyTickets(idc)),
                );
              },
            )
          ],
        ));
  }
}
