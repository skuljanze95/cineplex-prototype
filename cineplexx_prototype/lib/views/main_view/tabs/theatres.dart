import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

import 'package:graphql_flutter/graphql_flutter.dart';

class TheatresView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final HttpLink link =
        HttpLink(uri: 'https://cineplexx-prototype.herokuapp.com/v1/graphql');

    final ValueNotifier<GraphQLClient> client = ValueNotifier<GraphQLClient>(
      GraphQLClient(
        link: link,
        cache: OptimisticCache(
          dataIdFromObject: typenameDataIdFromObject,
        ),
      ),
    );
    return GraphQLProvider(
      client: client,
      child: CacheProvider(
        child: Theatres_G(),
      ),
    );
  }
}

class Theatres_G extends StatefulWidget {
  @override
  _Theatres_GState createState() => _Theatres_GState();
}

class _Theatres_GState extends State<Theatres_G> {
  String query = '''
  query MyQuery {
 
  theatres {
    name
    address
    phone_number
    email
    picture
  }
}
  ''';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            color: Color(0xffFCFCFC),
            height: MediaQuery.of(context).size.height * 0.9,
            child: Query(
              options: QueryOptions(
                documentNode:
                    gql(query), // this is the query string you just created

                pollInterval: 1000,
              ),
              builder: (QueryResult result,
                  {VoidCallback refetch, FetchMore fetchMore}) {
                if (result.hasException) {
                  return Text(result.exception.toString());
                }

                if (result.loading) {
                  return Center(
                    child: const CircularProgressIndicator(),
                  );
                }

                return ListView.builder(
                  itemCount: result.data['theatres'].length,
                  itemBuilder: (BuildContext context, int index) {
                    return Column(
                      children: <Widget>[
                        FutureBuilder(
                            future: buildNameText(result.data, index),
                            builder: (BuildContext context,
                                AsyncSnapshot<Widget> snapshot) {
                              if (snapshot.hasData) return snapshot.data;

                              return Container();
                            })
                      ],
                    );
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Future<Container> buildNameText(payload, int index) async {
    var name, address, number, email, img;

    name = payload['theatres'][index]['name'];
    img = payload['theatres'][index]['picture'];
    address = payload['theatres'][index]['address'];
    number = payload['theatres'][index]['phone_number'];
    email = payload['theatres'][index]['email'];

    return Container(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(11.0, 0, 11.0, 25.0),
        child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black12,
                    blurRadius: 18.0, // has the effect of softening the shadow
                    spreadRadius: 3.0, // has the effect of extending the shadow
                    offset: Offset(
                      0, // horizontal, move right 10
                      6.0, // vertical, move down 10
                    ),
                  )
                ]),
            height: 150,
            child: Row(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(right: 8.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: const Radius.circular(12.0),
                      bottomLeft: const Radius.circular(12.0),
                    ),
                    child: Image.network(
                      "$img",
                      width: 170,
                      height: 150,
                      fit: BoxFit.fitHeight,
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.50,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: AutoSizeText(
                          name.toString(),
                          maxLines: 2,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: 14.0,
                            //You can set your custom height here
                          ),
                        ),
                      ),
                      buildTheaterText(address),
                      buildTheaterText(number),
                      buildTheaterText(email),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Icon(Icons.camera_alt),
                          Icon(Icons.camera_alt),
                          Icon(Icons.camera_alt),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            )),
      ),
    );
  }

  buildTheaterText(address) {
    return AutoSizeText(
      address.toString(),
      maxLines: 2,
      textAlign: TextAlign.start,
      style: TextStyle(
        fontSize: 11.0,
        //You can set your custom height here
      ),
    );
  }
}
