import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

import 'package:graphql_flutter/graphql_flutter.dart';

class NewsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final HttpLink link =
        HttpLink(uri: 'https://cineplexx-prototype.herokuapp.com/v1/graphql');

    final ValueNotifier<GraphQLClient> client = ValueNotifier<GraphQLClient>(
      GraphQLClient(
        link: link,
        cache: OptimisticCache(
          dataIdFromObject: typenameDataIdFromObject,
        ),
      ),
    );
    return GraphQLProvider(
      client: client,
      child: CacheProvider(
        child: NewsView_G(),
      ),
    );
  }
}

class NewsView_G extends StatefulWidget {
  @override
  _NewsView_GState createState() => _NewsView_GState();
}

class _NewsView_GState extends State<NewsView_G> {
  String query = '''
  query MyQuery {
 
  news {
    picture
    news_text
    title
  }
}
  ''';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            color: Color(0xffFCFCFC),
            height: MediaQuery.of(context).size.height * 0.9,
            child: Query(
              options: QueryOptions(
                documentNode:
                    gql(query), // this is the query string you just created

                pollInterval: 1000,
              ),
              builder: (QueryResult result,
                  {VoidCallback refetch, FetchMore fetchMore}) {
                if (result.hasException) {
                  return Text(result.exception.toString());
                }

                if (result.loading) {
                  return Center(
                    child: const CircularProgressIndicator(),
                  );
                }

                return ListView.builder(
                  itemCount: result.data['news'].length,
                  itemBuilder: (BuildContext context, int index) {
                    return Column(
                      children: <Widget>[
                        FutureBuilder(
                            future: buildNameText(result.data, index),
                            builder: (BuildContext context,
                                AsyncSnapshot<Widget> snapshot) {
                              if (snapshot.hasData) return snapshot.data;

                              return Container();
                            })
                      ],
                    );
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Future<Container> buildNameText(payload, int index) async {
    var title, news_text, img;

    title = payload['news'][index]['title'];
    news_text = payload['news'][index]['news_text'];
    img = payload['news'][index]['picture'];

    return Container(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(11.0, 0, 11.0, 25.0),
        child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black12,
                    blurRadius: 18.0, // has the effect of softening the shadow
                    spreadRadius: 3.0, // has the effect of extending the shadow
                    offset: Offset(
                      0, // horizontal, move right 10
                      6.0, // vertical, move down 10
                    ),
                  )
                ]),
            height: 250,
            child: Row(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(right: 8.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: const Radius.circular(12.0),
                      bottomLeft: const Radius.circular(12.0),
                    ),
                    child: Image.network(
                      "$img",
                      width: 170,
                      height: 270,
                      fit: BoxFit.fitHeight,
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.50,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: AutoSizeText(
                          title.toString(),
                          maxLines: 2,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: 18.0,
                            //You can set your custom height here
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: AutoSizeText(
                          news_text.toString(),
                          maxLines: 9,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: 14.0,
                            //You can set your custom height here
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          RaisedButton(
                            elevation: 0.0,
                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(8.0),
                              side: BorderSide(color: Colors.red),
                            ),
                            child: Text(
                              "Read more",
                              style: TextStyle(color: Colors.red),
                            ),
                            onPressed: () {},
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            )),
      ),
    );
  }
}
