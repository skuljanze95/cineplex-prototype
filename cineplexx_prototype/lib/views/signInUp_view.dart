import 'package:cineplexx_prototype/widgets/provider.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';

class SignInUp extends StatefulWidget {
  @override
  _SignInUpState createState() => _SignInUpState();
}

bool stateSignIn = true;

String _first_name,
    _last_name,
    _mobile_number,
    _email,
    _password,
    _emailS,
    _passwordS,
    _confirm_password;

class _SignInUpState extends State<SignInUp> {
  void validate(String _pass, _confirm_pass) {
    if (_pass == _confirm_pass) {
      submit();
    } else {
      Flushbar(
        title: "GUD VERI GUD",
        message: "NOICE",
        duration: Duration(seconds: 3),
      )..show(context);
    }
  }

  void submit() async {
    try {
      final auth = Provider.of(context).auth;
      if (stateSignIn == true) {
        FocusScope.of(context).requestFocus(new FocusNode());
        String uid = await auth.signInWithEmailAndPassword(_emailS, _passwordS);
        print("Singed in with $uid");
        Navigator.of(context).pushReplacementNamed("/home");
      } else {
        FocusScope.of(context).requestFocus(new FocusNode());
        String uid = await auth.createUserWithEmailAndPassword(
            _first_name, _last_name, _mobile_number, _email, _password);
        print("Singed in with $uid");
        Navigator.of(context).pushReplacementNamed("/home");
      }
    } catch (error) {
      switch (error.code) {
        case "ERROR_INVALID_EMAIL":
          Flushbar(
            title: "Error",
            message: "Your email address appears to be malformed.",
            duration: Duration(seconds: 3),
          )..show(context);

          break;
        case "ERROR_WRONG_PASSWORD":
          Flushbar(
            title: "Error",
            message: "Your password is wrong.",
            duration: Duration(seconds: 3),
          )..show(context);

          break;
        case "ERROR_USER_NOT_FOUND":
          Flushbar(
            title: "Error",
            message: "User with this email doesn't exist.",
            duration: Duration(seconds: 3),
          )..show(context);

          break;
        case "ERROR_USER_DISABLED":
          Flushbar(
            title: "Error",
            message: "User with this email has been disabled.",
            duration: Duration(seconds: 3),
          )..show(context);

          break;
        case "ERROR_TOO_MANY_REQUESTS":
          Flushbar(
            title: "Error",
            message: "Too many requests. Try again later.",
            duration: Duration(seconds: 3),
          )..show(context);

          break;
        case "ERROR_OPERATION_NOT_ALLOWED":
          Flushbar(
            title: "Error",
            message: "Signing in with Email and Password is not enabled.",
            duration: Duration(seconds: 3),
          )..show(context);

          break;
        default:
          Flushbar(
            title: "Error",
            message: "An undefined Error happened.",
            duration: Duration(seconds: 3),
          )..show(context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          color: const Color(0xffFCFCFC),
          child: Column(
            children: <Widget>[
              Flexible(
                child: ListView(
                  children: <Widget>[
                    buildTittle(),
                    buildInput(),
                  ],
                ),
              ),
              buildSwitch(),
            ],
          ),
        ),
      ),
    );
  }

  buildInput() {
    if (stateSignIn == true) {
      return Padding(
        padding: const EdgeInsets.all(18.0),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 15.0, // has the effect of softening the shadow
              spreadRadius: 6.0, // has the effect of extending the shadow
              offset: Offset(
                0, // horizontal, move right 10
                8.0, // vertical, move down 10
              ),
            )
          ], borderRadius: BorderRadius.circular(15), color: Colors.white),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              buildInputName("Email"),
              buildInputField("_emailS", "Enter email address", false, 22.0,
                  22.0, TextInputType.emailAddress),
              buildInputName("Password"),
              buildInputField("_passwordS", "Enter password", true, 22.0, 22.0,
                  TextInputType.text),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  FlatButton(
                    onPressed: () {},
                    child: Text("Forgot Password?"),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(25.0, 0, 25.0, 25.0),
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(8.0),
                  ),
                  color: const Color(0xffFE2E2E),
                  child: Padding(
                    padding: const EdgeInsets.only(top: 15.0, bottom: 15.0),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(boxShadow: [
                        BoxShadow(
                          color: Color.fromRGBO(254, 46, 46, 0.15),
                          blurRadius:
                              18.0, // has the effect of softening the shadow
                          spreadRadius:
                              3.0, // has the effect of extending the shadow
                          offset: Offset(
                            0, // horizontal, move right 10
                            20.0, // vertical, move down 10
                          ),
                        )
                      ]),
                      child: Text(
                        "Sign In",
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  onPressed: () {
                    submit();
                  },
                ),
              ),
            ],
          ),
        ),
      );
    } else {
      return Padding(
        padding: const EdgeInsets.all(18.0),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 15.0, // has the effect of softening the shadow
              spreadRadius: 6.0, // has the effect of extending the shadow
              offset: Offset(
                0, // horizontal, move right 10
                8.0, // vertical, move down 10
              ),
            )
          ], borderRadius: BorderRadius.circular(15), color: Colors.white),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width * 0.43,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        buildInputName("First name"),
                        buildInputField("_first_name", "Enter first name",
                            false, 22.0, 0.0, TextInputType.text),
                      ],
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.43,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 3, top: 22.0, bottom: 8.0),
                          child: Text("Last name"),
                        ),
                        buildInputField("_last_name", "Enter last name", false,
                            0.0, 22.0, TextInputType.text),
                      ],
                    ),
                  )
                ],
              ),
              buildInputName("Mobile number"),
              buildInputField("_mobile_number", "Enter your number", false,
                  22.0, 22.0, TextInputType.phone),
              buildInputName("Email"),
              buildInputField("_email", "Enter email address", false, 22.0,
                  22.0, TextInputType.emailAddress),
              buildInputName("Password"),
              buildInputField("_password", "Enter password", true, 22.0, 22.0,
                  TextInputType.text),
              buildInputName("Confirm Password"),
              buildInputField("_confirm_password", "Enter password", true, 22.0,
                  22.0, TextInputType.text),
              Padding(
                padding: const EdgeInsets.all(25.0),
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(8.0),
                  ),
                  color: const Color(0xffFE2E2E),
                  child: Padding(
                    padding: const EdgeInsets.only(top: 15.0, bottom: 15.0),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(boxShadow: [
                        BoxShadow(
                          color: Color.fromRGBO(254, 46, 46, 0.15),
                          blurRadius:
                              18.0, // has the effect of softening the shadow
                          spreadRadius:
                              3.0, // has the effect of extending the shadow
                          offset: Offset(
                            0, // horizontal, move right 10
                            20.0, // vertical, move down 10
                          ),
                        )
                      ]),
                      child: Text(
                        "Sign Up",
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  onPressed: () {
                    validate(_password, _confirm_password);
                  },
                ),
              ),
            ],
          ),
        ),
      );
    }
  }

  buildInputField(name, hint, obscureText, left, right, keyboardType) {
    return Padding(
      padding: EdgeInsets.only(left: left, right: right),
      child: TextFormField(
        keyboardType: keyboardType,
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
          hintText: hint,
          focusColor: Colors.grey,
          focusedBorder: OutlineInputBorder(
              borderRadius: const BorderRadius.all(
                const Radius.circular(9.0),
              ),
              borderSide: BorderSide(color: Colors.grey)),
          border: OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(9.0),
            ),
            borderSide: BorderSide(color: Colors.grey),
          ),
        ),
        obscureText: obscureText,
        onChanged: (value) {
          setState(() {
            if (name == "_first_name") {
              _first_name = value;
            } else if (name == "_last_name") {
              _last_name = value;
            } else if (name == "_mobile_number") {
              _mobile_number = value;
            } else if (name == "_email") {
              _email = value;
            } else if (name == "_password") {
              _password = value;
            } else if (name == "_confirm_password") {
              _confirm_password = value;
            } else if (name == "_passwordS") {
              _passwordS = value;
            } else if (name == "_emailS") {
              _emailS = value;
            }
          });
        },
      ),
    );
  }

  buildInputName(String name) {
    return Padding(
      padding: const EdgeInsets.only(left: 25.0, top: 22.0, bottom: 8.0),
      child: Text(name),
    );
  }

  buildTittle() {
    if (stateSignIn == true) {
      return Center(
          child: Padding(
        padding: const EdgeInsets.only(top: 18.0),
        child: Text("Sign In"),
      ));
    } else {
      return Center(
          child: Padding(
        padding: const EdgeInsets.only(top: 18.0),
        child: Text("Sign Up"),
      ));
    }
  }

  buildSwitch() {
    if (stateSignIn == true) {
      if (MediaQuery.of(context).viewInsets.bottom < 1) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Don't have an account?"),
            FlatButton(
              padding: EdgeInsets.all(0),
              onPressed: () {
                setState(() {
                  stateSignIn = !stateSignIn;
                });
              },
              child: Text("Sign Up"),
            )
          ],
        );
      } else {
        return SizedBox(
          height: 0.0,
        );
      }
    } else {
      if (MediaQuery.of(context).viewInsets.bottom < 1) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Already have an account?"),
            FlatButton(
              padding: EdgeInsets.all(0),
              onPressed: () {
                setState(() {
                  stateSignIn = !stateSignIn;
                });
              },
              child: Text("Sign In"),
            )
          ],
        );
      } else {
        return SizedBox(
          height: 0.0,
        );
      }
    }
  }
}
