import 'package:cineplexx_prototype/services/auth.dart';
import 'package:cineplexx_prototype/views/first_view.dart';
import 'package:cineplexx_prototype/views/main_view/main_view.dart';

import 'package:cineplexx_prototype/views/signInUp_view.dart';

import 'package:cineplexx_prototype/widgets/provider.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Provider(
      auth: AuthService(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(),
        home: HomeControler(),
        routes: <String, WidgetBuilder>{
          "/singIn": (BuildContext context) => SignInUp(),
          "/home": (BuildContext context) => HomeControler(),
        },
        //navigatorObservers: [routeObserver],
      ),
    );
  }
}

class HomeControler extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final AuthService auth = Provider.of(context).auth;
    return StreamBuilder(
      stream: auth.onAuthStateChanged,
      builder: (context, AsyncSnapshot<String> snapshot) {
        if (snapshot.connectionState == ConnectionState.active) {
          final bool singgedIn = snapshot.hasData;
          return singgedIn ? MainView() : FirstView();
        }
        return CircularProgressIndicator();
      },
    );
  }
}
