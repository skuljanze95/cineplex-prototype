# Cineplex prototype

Android Flutter prototype app in which you can see which movies are currently playing, buy or reserve tickets, see which movies are coming soon, find theatres close to you, check news about actors and movies and more

(App is not published or finished)

Technologies:
- Flutter
- Firebase
- GrapQL


![Image description](https://i.ibb.co/44Mb2NQ/Group-11.png)
![Image description](https://i.ibb.co/2shDBmh/Group-10.png)